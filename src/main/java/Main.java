import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import project.Products;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nicklister on 04/03/2017.
 */
public class Main {


    public static void main(String[] args) throws IOException {
        Products product = new Products();

        product.connect("http://www.argos.co.uk/search/green-sofa");
        product.addPriceToProductRepo("price-now");
        product.addNameToProductRepo("product-title");

        product.addSKUToProductRepo("data-product-id");
        product.retrieveProductById(1);

        product.retrieveAllProducts();
        }
    }