package project;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by nicklister on 04/03/2017.
 */
public class Products {
    private String sku;
    private String name;
    private String price;
    private HashMap<Integer, Products> hash = new HashMap<Integer, Products>();
    private int count = 0;
    private static Document connection;

    public Products() {

    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Document connect(String site) throws IOException {
        connection = Jsoup.connect(site).get();
        return connection;
    }

    public boolean checkHashLength() {
        if (hash.isEmpty()) {
            return false;
        }
        return true;
    }

    public void addNameToProductRepo(String className) throws IOException {
        Elements products = Products.connection.getElementsByClass(className);
        count = 0;
        if (!checkHashLength()) {
            for (Element product : products) {
                Products a = new Products();
                a.setName(product.text());
                hash.put(count, a);
                count++;
            }
        } else {
            for (Element product : products) {
                hash.get(count).setName(product.text());
                count++;
            }
        }
        count = 0;
    }

    public void addPriceToProductRepo(String className) throws IOException {
        Elements products = Products.connection.getElementsByClass(className);
        count = 0;
        if (!checkHashLength()) {
            for (Element product : products) {
                Products a = new Products();
                a.setPrice(product.text());
                hash.put(count, a);
                count++;
            }
        } else {
            for (Element product : products) {
                hash.get(count).setPrice(product.text());
                count++;
            }
        }

        count = 0;
    }

    public void addSKUToProductRepo(String attribute) throws IOException {
        Elements products = Products.connection.getElementsByAttribute(attribute);
        count = 0;
        if (!checkHashLength()) {
            for (Element product : products) {
                Products a = new Products();
                a.setSku(product.attr(attribute));
                hash.put(count, a);
                count++;
            }
        } else {
            for (Element product : products) {
                hash.get(count).setSku(product.attr(attribute));
                count++;
            }
        }
        count = 0;
    }


    public void retrieveProductById(int key) {
        if (hash.size() > key) {
            System.out.println("SKU: " + hash.get(key).getSku() + ". Product Name: " + hash.get(key).getName() + " Price: " + hash.get(key).getPrice());
        } else
            System.out.println("Sorry, that product doesn't exist.");
    }

    public void retrieveAllProducts() {
        count = 0;
        for (Integer a : hash.keySet()) {
            System.out.print("SKU: " + hash.get(count).getSku());
            System.out.print(". Name: " + hash.get(count).getName());
            System.out.println(" Price: " + hash.get(count).getPrice());
            count++;
        }
    }
}
